import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
from hats_rest.models import LocationVO


def get_locations():
    locations = requests.get("http://wardrobe-api:8000/api/locations/")
    data = json.loads(locations.content)
    for location in data["locations"]:
        LocationVO.objects.update_or_create(
            id=location["id"],
            defaults={
                "import_href": location["href"],
                "closet_name": location["closet_name"],
                "section_number": location["section_number"],
                "shelf_number": location["shelf_number"],
            },
        )


def poll():
    while True:
        print("Hats poller polling for data")
        try:
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(20)


if __name__ == "__main__":
    poll()
