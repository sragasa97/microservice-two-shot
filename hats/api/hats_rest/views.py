from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

from .models import LocationVO, Hat


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "id", "closet_name", "section_number", "shelf_number"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "style_name", "picture_url"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color", "picture_url", "location"]
    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_hats(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, items = Hat.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": {"successful?": count > 0, "count": count, "items": items}}
        )
    else:
        content = json.loads(request.body)

        if "location" in content:
            try:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
            except LocationVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Location ID"})

        Hat.objects.filter(id=id).update(**content)

        hat = Hat.objects.get(id=id)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
