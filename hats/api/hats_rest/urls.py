from django.urls import path

from .views import (
    api_list_hats,
    api_detail_hats,
)

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:id>/", api_detail_hats, name="api_detail_hats"),
]
