import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './hats/HatForm';
import HatsList from './hats/HatsList';
import HatDetail from './hats/HatDetail';
import ShoesList from './shoes/ShoesList'
import ShoesForm from './shoes/ShoesForm'

function App(props) {
  // if ((props.hats === undefined) || (props.shoes === undefined) ) {
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage/>}/>
          <Route path="hats">
            <Route index element={<HatsList hats={props.hats}/>}/>
            {/* <Route path=":id" element={<HatDetail/>}/> */}
            <Route path="new" element={<HatForm/>}/>
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList shoe = {props.shoes} />}/>
            <Route path="new" element={<ShoesForm/>}/>

          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
