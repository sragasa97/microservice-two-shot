import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ShoesColumn1(props) {
  return (
    <div className="col">
      {props.shoe.map(data => {
        const shoe = data;
        return (
          <div key={shoe.href} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{shoe.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {shoe.manufacturer}
              </h6>
              <p className="card-text">
                {shoe.bin}
              </p>
            </div>
            <div className="card-footer">
              {shoe.color}
            </div>
          </div>
        );
      })}
    </div>
  );
}

const ShoesList = (props) => {
  const [shoeColumn, setShoeColumn] = useState([], [], []);


  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/'

    try {
      const response = await fetch(url);
      if (response.ok) {
        const requests = [];

  //         // Set up the "columns" to put the conference
  //         // information into
        const columns = [[], [], []];

  //    // Loop over the conference detail responses and add
  //    // each to to the proper "column" if the response is
  //    // ok
        let i = 0;
        const data = await response.json()
        for (const shoe of data) {
          columns[i].push(shoe);
          i = i + 1;
          if (i > 2) {
            i = 0;
          } else {
            console.error(shoe);
          }
          setShoeColumn(columns)
        }
      }
    } catch (e) {
        console.error(e);
      }
  }

  useEffect(() => {
    fetchData();
  }, []);
  return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Wardrobify!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              An effective organization system for manageing a large closet.
            </p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            </div>
          </div>
        </div>
        <div className="container">
          <h2>Shoes Closet</h2>
          <div className="row">
            {shoeColumn.map((shoesList, index) => {
              return (
                <ShoesColumn1 key={index} shoe={shoesList} />
              );
            })}
          </div>
        </div>
      </>
    );
}

export default ShoesList;
