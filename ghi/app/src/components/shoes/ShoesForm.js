import React, {useState, useEffect} from 'react';

function ShoesForm() {
  const [shoes, setShoes] = useState([])
  const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    id: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8080/api/shoes/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        id: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Catalog a new shoe</h1>
          <form onSubmit={handleSubmit} id="catalog-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="name">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.model_name} placeholder="Model name" required type="text" name="Model name" id="model name" className="form-control" />
              <label htmlFor="starts">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="ends">Color</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Picture</label>
              <input onChange={handleFormChange} value={formData.picture_url} placeholder = "picture url" required type="text" id="picture url" name="picture url" className="form-control"/>
            </div>
                {shoes.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                  )
                })}
          </form>
            </div>
            <button className="btn btn-primary">Create</button>
        </div>
      </div>
  );
}

export default ShoesForm;
