import React, {useState, useEffect} from 'react';


function HatColumn(props) {
    const [refresh, setRefresh] = useState({})

    async function handleDelete(href) {

        const deleteURL = `http://localhost:8090${href}`

        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(deleteURL, fetchConfig)
        const data = await response.json();

        console.log(data)
        setRefresh(data)

    }

    return (
        <div className="col">
            {props.hats.map(data => {
                const hat = data;
                console.log(hat)
                    return (
                        <div key={hat.href} className="card mb-3 shadow">
                        <img src={hat.picture_url} alt="a hat!" className="card-img-top" />
                        <div className="card-body">
                            <h5 className="card-title">{hat.style_name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                {hat.color} | {hat.fabric}
                            </h6>
                        </div>
                        <div className="card-footer">
                            <h5 className="card-title">Located: {hat.location.closet_name}.</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                Section {hat.location.section_number} | Shelf {hat.location.shelf_number}
                            </h6>
                            <div>
                                <button className="btn btn-primary">Detail Page</button>
                                <button className="btn invisible" id="dontClick">Hi</button>
                                <button onClick={handleDelete(hat.href)} className="btn btn-danger">Delete Hat</button>
                            </div>
                        </div>

                        </div>
                    );
                })
            }
        </div>
    )
}

const HatsList = (props) => {
    const [hatColumn, setHatColumn] = useState([], [], []);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/'

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (const hat of data.hats) {
                    const detailURL = `http://localhost:8090${hat.href}`
                    requests.push(fetch(detailURL));
                }

                const responses = await Promise.all(requests);

                const columns = [[], [], []];

                let counter = 0;

                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        columns[counter].push(details);

                        counter++;
                        if (counter === 3) {
                            counter = 0;
                        }

                    } else {
                        console.error(hatResponse)
                    }
                }

                setHatColumn(columns);
            }
        } catch (e) {
            console.error(e)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
          <div className="px-4 py-5 my-5 mt-50 text-center bg-info">
            <h1 className="display-2 fw-bold">WARDROBIFY!</h1>
            <div className="col-lg-6 mx-auto">
              <p className="lead mb-2">
                Need to keep track of your shoes and hats?
              </p>
              <p className="lead mb-2">
                We have the solution for you!
              </p>
            </div>
          </div>
          <div className="container">
            <h2>All Your Hats</h2>
            <div className="row">
              {hatColumn.map((hatList, index) => {
                return (
                  <HatColumn key={index} hats={hatList} />
                );
              })}
            </div>
          </div>
        </>
      );
}

export default HatsList;
