import React, {useState, useEffect} from 'react';

function HatForm() {
    const [fabric, setFabric] = useState('')
    const [style_name, setStyle] = useState('')
    const [color, setColor] = useState('')
    const [picture, setPicture] = useState('')
    const [location, setLocation] = useState('')
    const [locationForm, setLocationForm] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocationForm(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleFabricChange = (e) => {
        setFabric(e.target.value);
    };
    const handleStyleChange = (e) => {
        setStyle(e.target.value);
    };
    const handleColorChange = (e) => {
        setColor(e.target.value);
    };
    const handlePictureChange = (e) => {
        setPicture(e.target.value);
    };
    const handleLocationChange = (e) => {
        setLocation(e.target.value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            "fabric": fabric,
            "style_name": style_name,
            "color": color,
            "picture_url": picture,
            "location": location
        }

        const hatURL = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            setFabric('');
            setStyle('');
            setColor('');
            setPicture('');
            setLocation('');
        }
    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Hat To Your Collection!</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input value={style_name} onChange={handleStyleChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={picture} onChange={handlePictureChange} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                <label htmlFor="picture">Picture URL</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locationForm.map(location => {
                        return (<option key={location.href} value={location.id}>
                            {location.closet_name} - Section {location.section_number} / Shelf {location.shelf_number}
                        </option>)
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create Hat</button>
            </form>
          </div>
        </div>
      </div>

    );
}

export default HatForm;
