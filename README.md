# Wardrobify

Team:

* Shaun Ragasa | Hats Microservice
* Paula Mejia | Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The main model of the Hats microservice is the Hat model. It has the properties of fabric, style name, color, a picture, and a location.

Its location attribute is a foreign key to a Location Value Object derived from the location model found in the wardrobe microservice. The LocationVO model data is populated using polling with a 1:1 data value to the Location model.
