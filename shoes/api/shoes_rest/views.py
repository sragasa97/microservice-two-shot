from django.shortcuts import render
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder)
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(id=content["id"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid bin"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoesListEncoder,
            safe=False,
        )


@require_http_methods("DELETE")
def api_detail_shoe(request, id):
    if request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse({"message": count > 0})
