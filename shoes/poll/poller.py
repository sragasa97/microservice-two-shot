import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something

from shoes_rest.models import BinVO

def get_bins():
    bins = requests.get("http://wardrobe-api:8000/api/bins/")
    data = json.loads(bins.content)
    for bin in data["bins"]:
        BinVO.objects.update_or_create(
            id=bin["id"],
            defaults={
            "import_href":bin["href"],
            "closet_name": bin["closet_name"],
            "bin_number": bin["bin_number"],
            "bin_size": bin["bin_size"],
            })



def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
